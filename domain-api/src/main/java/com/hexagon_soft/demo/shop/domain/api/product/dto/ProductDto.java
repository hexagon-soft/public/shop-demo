package com.hexagon_soft.demo.shop.domain.api.product.dto;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductDto {

    private String name;
    private String catalogNumber;
    private BigDecimal cost;

}
