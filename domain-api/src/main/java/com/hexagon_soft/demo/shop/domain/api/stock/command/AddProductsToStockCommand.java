package com.hexagon_soft.demo.shop.domain.api.stock.command;

import com.hexagon_soft.demo.shop.domain.api.stock.ProductStockId;
import lombok.Value;
import org.apache.commons.lang3.Validate;

@Value
public class AddProductsToStockCommand {

    private ProductStockId productStockId;
    private int quantity;

    public AddProductsToStockCommand(ProductStockId productStockId, int quantity) {
        Validate.notNull(productStockId);
        Validate.isTrue(quantity > 0);

        this.productStockId = productStockId;
        this.quantity = quantity;
    }

}
