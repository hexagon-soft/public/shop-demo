package com.hexagon_soft.demo.shop.domain.api.product.command;

import java.math.BigDecimal;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

@Getter
public final class AddProductToOfferCommand {

    private final ProductId id;
    private final String name;
    private final String catalogNumber;
    private final BigDecimal cost;

    public AddProductToOfferCommand(ProductId id, String name, String catalogNumber, BigDecimal cost) {
        Validate.isTrue(cost.signum() > 0);

        this.id = Validate.notNull(id);
        this.name = Validate.notBlank(name);
        this.catalogNumber = Validate.notBlank(catalogNumber);
        this.cost = cost;
    }

}
