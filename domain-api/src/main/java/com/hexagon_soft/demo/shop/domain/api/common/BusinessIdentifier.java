package com.hexagon_soft.demo.shop.domain.api.common;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class BusinessIdentifier implements Serializable {

    private final UUID uuid;

    protected BusinessIdentifier() {
        this(UUID.randomUUID());
    }

    protected BusinessIdentifier(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        BusinessIdentifier that = (BusinessIdentifier) object;
        return Objects.equals(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }

    @Override
    public String toString() {
        return uuid.toString();
    }

}
