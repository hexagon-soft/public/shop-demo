package com.hexagon_soft.demo.shop.domain.api.stock.event;

import com.hexagon_soft.demo.shop.domain.api.stock.ProductStockId;
import lombok.Value;
import org.apache.commons.lang3.Validate;

@Value
public class ProductsAddedToStockEvent {

    private ProductStockId productStockId;
    private int oldQuantity;
    private int addedQuantity;

    public ProductsAddedToStockEvent(ProductStockId productStockId, int oldQuantity, int addedQuantity) {
        Validate.notNull(productStockId);
        Validate.isTrue(oldQuantity >= 0);
        Validate.isTrue(addedQuantity > 0);

        this.productStockId = productStockId;
        this.oldQuantity = oldQuantity;
        this.addedQuantity = addedQuantity;
    }

}
