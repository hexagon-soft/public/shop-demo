package com.hexagon_soft.demo.shop.domain.api.shopper.command;

import com.hexagon_soft.demo.shop.domain.api.shopper.ShopperId;
import lombok.Value;
import org.apache.commons.lang3.Validate;

@Value
public class AddShopperAccountCommand {

    private ShopperId shopperId;
    private String name;

    public AddShopperAccountCommand(ShopperId shopperId, String name) {
        this.shopperId = Validate.notNull(shopperId);
        this.name = Validate.notBlank(name);
    }

}
