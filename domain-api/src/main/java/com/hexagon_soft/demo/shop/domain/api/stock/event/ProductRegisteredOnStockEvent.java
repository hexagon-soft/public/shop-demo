package com.hexagon_soft.demo.shop.domain.api.stock.event;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import com.hexagon_soft.demo.shop.domain.api.stock.ProductStockId;
import lombok.Value;

@Value
public class ProductRegisteredOnStockEvent {

    private ProductStockId productStockId;
    private ProductId productId;

}
