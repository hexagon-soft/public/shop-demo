package com.hexagon_soft.demo.shop.domain.api.product.service;

import java.util.List;

import com.hexagon_soft.demo.shop.domain.api.product.dto.ProductDto;

public interface ProductService {

    boolean isAnyWithCatalogNumber(String catalogNumber);

    List<ProductDto> fetchAllProducts();

}
