package com.hexagon_soft.demo.shop.domain.api.product.event;

import java.math.BigDecimal;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import lombok.Value;
import org.apache.commons.lang3.Validate;

@Value
public class ProductAddedToOfferEvent {

    private final ProductId productId;
    private final String name;
    private final String catalogNumber;
    private final BigDecimal cost;

    public ProductAddedToOfferEvent(ProductId productId, String name, String catalogNumber, BigDecimal cost) {
        Validate.isTrue(cost.signum() > 0);

        this.productId = Validate.notNull(productId);
        this.name = Validate.notBlank(name);
        this.catalogNumber = Validate.notBlank(catalogNumber);
        this.cost = cost;
    }

}
