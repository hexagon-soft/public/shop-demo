package com.hexagon_soft.demo.shop.domain.api.shopper.event;

import com.hexagon_soft.demo.shop.domain.api.shopper.ShopperId;
import lombok.Value;
import org.apache.commons.lang3.Validate;

@Value
public class ShopperAccountAddedEvent {

    private ShopperId shopperId;
    private String name;

    public ShopperAccountAddedEvent(ShopperId shopperId, String name) {
        this.shopperId = Validate.notNull(shopperId);
        this.name = Validate.notBlank(name);
    }
    
}
