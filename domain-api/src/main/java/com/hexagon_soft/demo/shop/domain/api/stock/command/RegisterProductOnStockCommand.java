package com.hexagon_soft.demo.shop.domain.api.stock.command;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import com.hexagon_soft.demo.shop.domain.api.stock.ProductStockId;
import lombok.Value;

@Value
public class RegisterProductOnStockCommand {

    private ProductStockId productStockId;
    private ProductId productId;

}
