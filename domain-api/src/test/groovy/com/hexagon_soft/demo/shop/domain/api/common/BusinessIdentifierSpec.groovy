package com.hexagon_soft.demo.shop.domain.api.common

import spock.lang.Shared
import spock.lang.Specification

class BusinessIdentifierSpec extends Specification {

    @Shared
    def uuid = UUID.randomUUID()

    def "should check business identifiers equality"() {
        when:
        def result = id1 == id2

        then:
        result == expectedResult

        where:
        id1             | id2             || expectedResult
        new CatId()     | new CatId()     || false
        new CatId(uuid) | new CatId()     || false
        new CatId(uuid) | new CatId(uuid) || true
        new CatId(uuid) | new DogId(uuid) || false
    }

    class CatId extends BusinessIdentifier {
        CatId() {
        }

        CatId(UUID uuid) {
            super(uuid)
        }
    }

    class DogId extends BusinessIdentifier {
        DogId() {
        }

        DogId(UUID uuid) {
            super(uuid)
        }
    }

}
