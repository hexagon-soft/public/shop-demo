package com.hexagon_soft.demo.shop.initialization;

import java.math.BigDecimal;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import com.hexagon_soft.demo.shop.domain.api.product.command.AddProductToOfferCommand;
import com.hexagon_soft.demo.shop.domain.api.stock.command.AddProductsToStockCommand;
import com.hexagon_soft.demo.shop.query.product.ProductEntityRepository;
import com.hexagon_soft.demo.shop.query.stock.ProductStockEntity;
import com.hexagon_soft.demo.shop.query.stock.ProductStockEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ApplicationInitializer implements ApplicationListener<ApplicationReadyEvent> {

    private final CommandGateway commandGateway;
    private final ProductEntityRepository productEntryRepository;
    private final ProductStockEntityRepository productStockEntityRepository;

    public ApplicationInitializer(CommandGateway commandGateway, ProductEntityRepository productEntryRepository,
                                  ProductStockEntityRepository productStockEntityRepository) {

        this.commandGateway = commandGateway;
        this.productEntryRepository = productEntryRepository;
        this.productStockEntityRepository = productStockEntityRepository;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        if (productEntryRepository.count() == 0) {
            log.info("Products initialization ...");
            initProducts();
        }
    }

    private void initProducts() {
        initProduct("Lamp", "ref1", BigDecimal.TEN, 100);
        initProduct("Chair", "ref2", BigDecimal.valueOf(100), 20);
    }

    private void initProduct(String name, String catalogNumber, BigDecimal cost, int count) {
        ProductId productId = new ProductId();
        commandGateway.sendAndWait(new AddProductToOfferCommand(productId, name, catalogNumber, cost));

        ProductStockEntity productStock = productStockEntityRepository.findByProductBusinessId(productId);
        commandGateway.sendAndWait(new AddProductsToStockCommand(productStock.getBusinessId(), count));
    }

}
