package com.hexagon_soft.demo.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopApplication {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(ShopApplication.class, args);

        Thread.sleep(1000);
    }
}
