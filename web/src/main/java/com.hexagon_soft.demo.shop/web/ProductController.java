package com.hexagon_soft.demo.shop.web;

import java.util.List;

import com.hexagon_soft.demo.shop.domain.api.product.dto.ProductDto;
import com.hexagon_soft.demo.shop.domain.api.product.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/products")
class ProductController {

    private final ProductService productService;

    ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    List<ProductDto> fetchProducts() {
        return productService.fetchAllProducts();
    }

}
