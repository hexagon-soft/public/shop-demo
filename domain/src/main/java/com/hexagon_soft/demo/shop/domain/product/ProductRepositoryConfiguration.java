package com.hexagon_soft.demo.shop.domain.product;

import org.axonframework.commandhandling.model.Repository;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ProductRepositoryConfiguration {

    private EventStore eventStore;

    public ProductRepositoryConfiguration(EventStore eventStore) {
        this.eventStore = eventStore;
    }

    @Bean
    public Repository<Product> productRepository() {
        return new EventSourcingRepository<>(Product.class, eventStore);
    }

}
