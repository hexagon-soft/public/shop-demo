package com.hexagon_soft.demo.shop.domain.stock;

import org.axonframework.commandhandling.model.Repository;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ProductStockRepositoryConfiguration {

    private EventStore eventStore;

    ProductStockRepositoryConfiguration(EventStore eventStore) {
        this.eventStore = eventStore;
    }

    @Bean
    public Repository<ProductStock> productStockRepository() {
        return new EventSourcingRepository<>(ProductStock.class, eventStore);
    }

}
