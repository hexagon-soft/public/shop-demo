package com.hexagon_soft.demo.shop.domain.stock;

import com.hexagon_soft.demo.shop.domain.api.stock.command.AddProductsToStockCommand;
import com.hexagon_soft.demo.shop.domain.api.stock.command.RegisterProductOnStockCommand;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.Repository;
import org.springframework.stereotype.Component;

@Component
class ProductStockCommandHandler {

    private Repository<ProductStock> productStockRepository;

    public ProductStockCommandHandler(Repository<ProductStock> productStockRepository) {
        this.productStockRepository = productStockRepository;
    }

    @CommandHandler
    void handleRegisterProductOnStockCommand(RegisterProductOnStockCommand command) throws Exception {
        productStockRepository.newInstance(() ->
                new ProductStock(command.getProductStockId(), command.getProductId()));
    }

    @CommandHandler
    void handleAddProductsToStockCommand(AddProductsToStockCommand command) {
        int quantityToBeAdded = command.getQuantity();

        productStockRepository.load(command.getProductStockId().toString())
                .execute(productStock -> productStock.addQuantity(quantityToBeAdded));
    }

}
