package com.hexagon_soft.demo.shop.domain.stock;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import com.hexagon_soft.demo.shop.domain.api.stock.ProductStockId;
import com.hexagon_soft.demo.shop.domain.api.stock.event.ProductRegisteredOnStockEvent;
import com.hexagon_soft.demo.shop.domain.api.stock.event.ProductsAddedToStockEvent;
import org.apache.commons.lang3.Validate;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
class ProductStock {

    @AggregateIdentifier
    private ProductStockId id;
    private ProductId productId;
    private int available;

    private ProductStock() {
    }

    ProductStock(ProductStockId id, ProductId productId) {
        Validate.notNull(id);
        Validate.notNull(productId);

        apply(new ProductRegisteredOnStockEvent(id, productId));
    }

    void addQuantity(int quantityToBeAdded) {
        if (quantityToBeAdded <= 0) {
            throw new IllegalArgumentException("Cannot add not positive value");
        }

        apply(new ProductsAddedToStockEvent(id, available, quantityToBeAdded));
    }

    @EventSourcingHandler
    void onProductRegisteredOnStock(ProductRegisteredOnStockEvent event) {
        this.id = event.getProductStockId();
        this.productId = event.getProductId();
    }

    @EventSourcingHandler
    void onProductsAddedToStock(ProductsAddedToStockEvent event) {
        this.available += event.getAddedQuantity();
    }

}
