package com.hexagon_soft.demo.shop.domain.stock.listener;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import com.hexagon_soft.demo.shop.domain.api.product.event.ProductAddedToOfferEvent;
import com.hexagon_soft.demo.shop.domain.api.stock.ProductStockId;
import com.hexagon_soft.demo.shop.domain.api.stock.command.RegisterProductOnStockCommand;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Component
class ProductStockListener {

    private final CommandGateway commandGateway;

    public ProductStockListener(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @EventHandler
    void onProductAddedToOffer(ProductAddedToOfferEvent event) {
        ProductId productId = event.getProductId();
        ProductStockId productStockId = new ProductStockId();

        RegisterProductOnStockCommand command = new RegisterProductOnStockCommand(productStockId, productId);
        commandGateway.sendAndWait(command);
    }

}
