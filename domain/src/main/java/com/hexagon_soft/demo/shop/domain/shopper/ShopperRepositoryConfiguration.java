package com.hexagon_soft.demo.shop.domain.shopper;

import org.axonframework.commandhandling.model.Repository;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ShopperRepositoryConfiguration {

    private EventStore eventStore;

    public ShopperRepositoryConfiguration(EventStore eventStore) {
        this.eventStore = eventStore;
    }

    @Bean
    public Repository<Shopper> shopperRepository() {
        return new EventSourcingRepository<>(Shopper.class, eventStore);
    }

}
