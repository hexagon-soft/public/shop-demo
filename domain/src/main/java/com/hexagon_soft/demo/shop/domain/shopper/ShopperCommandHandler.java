package com.hexagon_soft.demo.shop.domain.shopper;

import com.hexagon_soft.demo.shop.domain.api.shopper.command.AddShopperAccountCommand;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.Repository;
import org.springframework.stereotype.Component;

@Component
class ShopperCommandHandler {

    private Repository<Shopper> shopperRepository;

    public ShopperCommandHandler(Repository<Shopper> shopperRepository) {
        this.shopperRepository = shopperRepository;
    }

    @CommandHandler
    void handleAddShopperAccount(AddShopperAccountCommand command) throws Exception {
        shopperRepository.newInstance(() ->
                new Shopper(command.getShopperId(), command.getName()));
    }

}
