package com.hexagon_soft.demo.shop.domain.product;

import static java.lang.String.format;

import com.hexagon_soft.demo.shop.domain.api.product.command.AddProductToOfferCommand;
import com.hexagon_soft.demo.shop.domain.api.product.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.Repository;
import org.springframework.stereotype.Component;

@Slf4j
@Component
class ProductCommandHandler {

    private final Repository<Product> productRepository;
    private final ProductService productService;

    ProductCommandHandler(Repository<Product> productRepository, ProductService productService) {
        this.productRepository = productRepository;
        this.productService = productService;
    }

    @CommandHandler
    void handleAddProductToOffer(AddProductToOfferCommand command) throws Exception {
        validate(command);

        productRepository.newInstance(() ->
                new Product(command.getId(),
                        command.getName(),
                        command.getCatalogNumber(),
                        command.getCost()));
    }

    private void validate(AddProductToOfferCommand command) {
        String catalogNumber = command.getCatalogNumber();
        if (productService.isAnyWithCatalogNumber(catalogNumber)) {
            throw new RuntimeException(format("Product cannot be created because product with catalog number '%s' already exists", catalogNumber));
        }
    }

}
