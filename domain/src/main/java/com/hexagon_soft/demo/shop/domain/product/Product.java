package com.hexagon_soft.demo.shop.domain.product;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

import java.math.BigDecimal;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import com.hexagon_soft.demo.shop.domain.api.product.event.ProductAddedToOfferEvent;
import org.apache.commons.lang3.Validate;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
class Product {

    @AggregateIdentifier
    private ProductId id;
    private String name;
    private String catalogNumber;
    private BigDecimal cost;

    private Product(){
    }

    Product(ProductId productId, String name, String catalogNumber, BigDecimal cost) {
        Validate.notNull(productId);
        Validate.notBlank(name);
        Validate.notBlank(catalogNumber);
        Validate.isTrue(cost.signum() > 0);

        apply(new ProductAddedToOfferEvent(productId, name, catalogNumber, cost));
    }

    @EventSourcingHandler
    void onProductAddedToOffer(ProductAddedToOfferEvent event) {
        this.id = event.getProductId();
        this.name = event.getName();
        this.catalogNumber = event.getCatalogNumber();
        this.cost = event.getCost();
    }

}
