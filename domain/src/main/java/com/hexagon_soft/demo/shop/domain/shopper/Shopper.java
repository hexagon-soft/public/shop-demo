package com.hexagon_soft.demo.shop.domain.shopper;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

import com.hexagon_soft.demo.shop.domain.api.shopper.ShopperId;
import com.hexagon_soft.demo.shop.domain.api.shopper.event.ShopperAccountAddedEvent;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class Shopper {

    @AggregateIdentifier
    private ShopperId id;
    private String name;

    Shopper(ShopperId id, String name) {
        Validate.notNull(id);
        Validate.notBlank(name);

        apply(new ShopperAccountAddedEvent(id, name));
    }

    @EventSourcingHandler
    void onShopperAccountAdded(ShopperAccountAddedEvent event) {
        this.id = event.getShopperId();
        this.name = event.getName();
    }

}
