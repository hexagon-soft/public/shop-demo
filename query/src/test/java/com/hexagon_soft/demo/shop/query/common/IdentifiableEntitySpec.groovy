package com.hexagon_soft.demo.shop.query.common

import com.hexagon_soft.demo.shop.domain.api.common.BusinessIdentifier
import spock.lang.Shared
import spock.lang.Specification

class IdentifiableEntitySpec extends Specification {

    @Shared
    def uuid = UUID.randomUUID()

    def "should compare entities"() {
        when:
        def result = entity1 == entity2

        then:
        result == expectedResult

        where:
        entity1                  | entity2                  || expectedResult
        new Cat(new CatId())     | new Cat(new CatId())     || false
        new Cat(new CatId(uuid)) | new Cat(new CatId(uuid)) || true
        new Cat(new CatId(uuid)) | new Dog(new DogId(uuid)) || false
    }

    class Cat extends IdentifiableEntity<CatId> {
        Cat(CatId businessId) {
            super(businessId)
        }
    }

    class Dog extends IdentifiableEntity<DogId> {
        Dog(DogId businessId) {
            super(businessId)
        }
    }

    class CatId extends BusinessIdentifier {
        CatId() {
        }

        CatId(UUID uuid) {
            super(uuid)
        }
    }

    class DogId extends BusinessIdentifier {
        DogId(UUID uuid) {
            super(uuid)
        }
    }
}
