package com.hexagon_soft.demo.shop.query.product;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import com.hexagon_soft.demo.shop.query.common.IdentifiableEntity;
import lombok.Getter;

@Getter
@Entity
@Table(name = "product")
public class ProductEntity extends IdentifiableEntity<ProductId> {

    @Column(nullable = false)
    private String name;

    @Column(name = "catalog_number", nullable = false)
    private String catalogNumber;

    @Column(nullable = false)
    private BigDecimal cost;

    private ProductEntity() {
    }

    ProductEntity(ProductId businessId, String name, String catalogNumber, BigDecimal cost) {
        super(businessId);
        this.name = name;
        this.cost = cost;
        this.catalogNumber = catalogNumber;
    }

}
