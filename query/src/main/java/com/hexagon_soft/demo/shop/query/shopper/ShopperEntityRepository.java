package com.hexagon_soft.demo.shop.query.shopper;

import com.hexagon_soft.demo.shop.domain.api.shopper.ShopperId;
import com.hexagon_soft.demo.shop.query.common.IdentifiableEntityRepository;

public interface ShopperEntityRepository extends IdentifiableEntityRepository<ShopperEntity, ShopperId> {

}
