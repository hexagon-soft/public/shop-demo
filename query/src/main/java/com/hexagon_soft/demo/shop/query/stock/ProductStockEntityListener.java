package com.hexagon_soft.demo.shop.query.stock;

import javax.transaction.Transactional;

import com.hexagon_soft.demo.shop.domain.api.stock.event.ProductRegisteredOnStockEvent;
import com.hexagon_soft.demo.shop.domain.api.stock.event.ProductsAddedToStockEvent;
import com.hexagon_soft.demo.shop.query.product.ProductEntity;
import com.hexagon_soft.demo.shop.query.product.ProductEntityRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Component
class ProductStockEntityListener {

    private ProductStockEntityRepository productStockEntityRepository;
    private ProductEntityRepository productEntityRepository;

    public ProductStockEntityListener(ProductStockEntityRepository productStockEntityRepository, ProductEntityRepository productEntityRepository) {
        this.productStockEntityRepository = productStockEntityRepository;
        this.productEntityRepository = productEntityRepository;
    }


    @EventHandler
    void onProductRegisteredOnStock(ProductRegisteredOnStockEvent event) {
        ProductEntity product = productEntityRepository.findByBusinessId(event.getProductId());
        ProductStockEntity productStock = new ProductStockEntity(event.getProductStockId(), product);

        productStockEntityRepository.save(productStock);
    }

    @EventHandler
    @Transactional
    void onProductsAddedToStock(ProductsAddedToStockEvent event) {
        ProductStockEntity stock = productStockEntityRepository.findByBusinessId(event.getProductStockId());
        stock.addToAvailable(event.getAddedQuantity());
    }

}
