package com.hexagon_soft.demo.shop.query.shopper;

import com.hexagon_soft.demo.shop.domain.api.shopper.event.ShopperAccountAddedEvent;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Component
class ShopperEntityListener {

    private ShopperEntityRepository shopperEntityRepository;

    public ShopperEntityListener(ShopperEntityRepository shopperEntityRepository) {
        this.shopperEntityRepository = shopperEntityRepository;
    }

    @EventHandler
    void onShopperAccountAdded(ShopperAccountAddedEvent event) {
        ShopperEntity shopper = new ShopperEntity(event.getShopperId(), event.getName());
        shopperEntityRepository.save(shopper);
    }

}
