package com.hexagon_soft.demo.shop.query.stock;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import com.hexagon_soft.demo.shop.domain.api.stock.ProductStockId;
import com.hexagon_soft.demo.shop.query.common.IdentifiableEntityRepository;

public interface ProductStockEntityRepository extends IdentifiableEntityRepository<ProductStockEntity, ProductStockId> {

    ProductStockEntity findByProductBusinessId(ProductId productId);

}
