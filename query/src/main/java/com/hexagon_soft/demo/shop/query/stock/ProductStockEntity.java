package com.hexagon_soft.demo.shop.query.stock;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hexagon_soft.demo.shop.domain.api.stock.ProductStockId;
import com.hexagon_soft.demo.shop.query.common.IdentifiableEntity;
import com.hexagon_soft.demo.shop.query.product.ProductEntity;
import org.apache.commons.lang3.Validate;

@Entity
@Table(name = "product_stock")
public class ProductStockEntity extends IdentifiableEntity<ProductStockId> {

    @OneToOne(optional = false)
    private ProductEntity product;

    private int available;

    private ProductStockEntity() {
    }

    ProductStockEntity(ProductStockId businessId, ProductEntity product) {
        super(businessId);
        this.product = product;
    }

    void addToAvailable(int quantity) {
        Validate.isTrue(quantity > 0);

        this.available += quantity;
    }

}
