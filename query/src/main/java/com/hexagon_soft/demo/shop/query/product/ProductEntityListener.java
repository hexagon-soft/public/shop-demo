package com.hexagon_soft.demo.shop.query.product;

import com.hexagon_soft.demo.shop.domain.api.product.event.ProductAddedToOfferEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Slf4j
@Component
class ProductEntityListener {

    private ProductEntityRepository repository;

    ProductEntityListener(ProductEntityRepository repository) {
        this.repository = repository;
    }

    @EventHandler
    void onProductAddedToOffer(ProductAddedToOfferEvent event) {
        ProductEntity product = new ProductEntity(
                event.getProductId(),
                event.getName(),
                event.getCatalogNumber(),
                event.getCost());

        repository.save(product);
    }

}
