package com.hexagon_soft.demo.shop.query.common;

import com.hexagon_soft.demo.shop.domain.api.common.BusinessIdentifier;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

@NoRepositoryBean
public interface IdentifiableEntityRepository<T extends IdentifiableEntity, ID extends BusinessIdentifier> extends Repository<T, Long> {

    T findByBusinessId(ID businessId);

    <S extends T> S save(S entity);

    long count();

}
