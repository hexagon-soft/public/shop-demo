package com.hexagon_soft.demo.shop.query.product;

import java.util.List;
import java.util.stream.Collectors;

import com.hexagon_soft.demo.shop.domain.api.product.dto.ProductDto;
import com.hexagon_soft.demo.shop.domain.api.product.service.ProductService;
import org.springframework.stereotype.Component;

@Component
class ProductEntityService implements ProductService {

    private ProductEntityRepository productEntityRepository;

    ProductEntityService(ProductEntityRepository productEntityRepository) {
        this.productEntityRepository = productEntityRepository;
    }

    @Override
    public boolean isAnyWithCatalogNumber(String catalogNumber) {
        return productEntityRepository.countByCatalogNumber(catalogNumber) > 0;
    }

    @Override
    public List<ProductDto> fetchAllProducts() {
        return productEntityRepository.findAll()
                .stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    private ProductDto toDto(ProductEntity product) {
        return ProductDto.builder()
                .name(product.getName())
                .catalogNumber(product.getCatalogNumber())
                .cost(product.getCost())
                .build();
    }
}
