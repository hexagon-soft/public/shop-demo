package com.hexagon_soft.demo.shop.query.product;

import java.util.List;

import com.hexagon_soft.demo.shop.domain.api.product.ProductId;
import com.hexagon_soft.demo.shop.query.common.IdentifiableEntityRepository;

public interface ProductEntityRepository extends IdentifiableEntityRepository<ProductEntity, ProductId> {

    Long countByCatalogNumber(String catalogNumber);

    List<ProductEntity> findAll();

}
