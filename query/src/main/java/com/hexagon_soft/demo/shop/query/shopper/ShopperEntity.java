package com.hexagon_soft.demo.shop.query.shopper;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.hexagon_soft.demo.shop.domain.api.shopper.ShopperId;
import com.hexagon_soft.demo.shop.query.common.IdentifiableEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.Validate;

@Entity
@Table(name = "shopper")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ShopperEntity extends IdentifiableEntity<ShopperId> {

    @Column(nullable = false)
    private String name;

    public ShopperEntity(ShopperId businessId, String name) {
        super(businessId);
        this.name = Validate.notBlank(name);
    }

}
