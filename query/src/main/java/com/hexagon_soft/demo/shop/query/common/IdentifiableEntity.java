package com.hexagon_soft.demo.shop.query.common;

import java.util.Objects;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.hexagon_soft.demo.shop.domain.api.common.BusinessIdentifier;
import org.hibernate.annotations.Type;

@MappedSuperclass
public abstract class IdentifiableEntity<I extends BusinessIdentifier> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Embedded
    @AttributeOverrides(@AttributeOverride(name = "uuid",
            column = @Column(name = "business_id", nullable = false, unique = true, updatable = false)))
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private I businessId;

    protected IdentifiableEntity() {
    }

    protected IdentifiableEntity(I businessId) {
        this.businessId = businessId;
    }

    public I getBusinessId() {
        return businessId;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        IdentifiableEntity<?> that = (IdentifiableEntity<?>) object;
        return Objects.equals(businessId, that.businessId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(businessId);
    }

}
